#Robert Higgins
#Project 5
#Version 1.0.0

from Character import Character
import Items
import Room
import random
import operator

N = "north"
E = "east"
S = "south"
W = "west"
DIRECTIONS = (N, E, S, W)
_4SR = 6
_3SR = 10
_2SR = 15
TR = _4SR + _3SR + _2SR

finalRooms = []

def getRoom(x, y) :
    #print("getRoom({}, {})".format(x,y))
    
    for room in finalRooms :
        if room.x == x and room.y == y : return room

    return None

def spawnRoom(x, y, availableRooms) :
    #print("spawnRoom({}, {}, availableRooms)".format(x,y))
    
    newRoom = random.randrange(len(availableRooms) + 7) #+7 to gen oneSidedRooms
        
    if newRoom >= len(availableRooms) : newRoom = Room.Room(1)
    else :
        newRoom = availableRooms.pop(newRoom)

    newRoom.x = x
    newRoom.y = y

    finalRooms.append(newRoom)
    finalRooms.sort(key=operator.attrgetter('x', 'y'))

    addExistingConnections(newRoom)

def addExistingConnections(room) :
    #print("addExistingConnections()")
    
    #n e s w
    adjacentRooms = [getRoom(room.x, room.y - 1), getRoom(room.x + 1, room.y), getRoom(room.x, room.y + 1), getRoom(room.x - 1, room.y)]
    numAdjacent = 0
    
    if adjacentRooms[0] :
        if len(room.connections) < room.doors and len(adjacentRooms[0].connections) < adjacentRooms[0].doors:
            room.connections[N] = adjacentRooms[0]
            adjacentRooms[0].connections[S] = room
        numAdjacent += 1
    
    if adjacentRooms[1] :
        if len(room.connections) < room.doors and len(adjacentRooms[1].connections) < adjacentRooms[1].doors:
            room.connections[E] = adjacentRooms[1]
            adjacentRooms[1].connections[W] = room
        numAdjacent += 1
            
    if adjacentRooms[2] :
        if len(room.connections) < room.doors and len(adjacentRooms[2].connections) < adjacentRooms[2].doors:
            room.connections[S] = adjacentRooms[2]
            adjacentRooms[2].connections[N] = room
        numAdjacent += 1

    if adjacentRooms[3] :
        if len(room.connections) < room.doors and len(adjacentRooms[3].connections) < adjacentRooms[3].doors:
            room.connections[W] = adjacentRooms[3]
            adjacentRooms[3].connections[E] = room
        numAdjacent += 1

    if numAdjacent == 4 :
        room.doors = -room.doors
        #negative to ensure that if this room has only one connection that it will not be replaced during single room replacement.
    
def createRoomRandomDirection(room, availableRooms) :
    #print("createRoomRandomDirection(room, availableRooms)")
    
    #while len(room.connections) < room.doors :
    d = random.randrange(len(DIRECTIONS))
    
    try :
        if room.connections[DIRECTIONS[d]] : return
    except KeyError : pass

    x = room.x
    y = room.y
    if DIRECTIONS[d] == N : y -= 1
    elif DIRECTIONS[d] == E : x += 1
    elif DIRECTIONS[d] == S : y += 1
    elif DIRECTIONS[d] == W : x -= 1
    
    if getRoom(x, y) : return

    newRoom = spawnRoom(x, y, availableRooms)

def searchForRoomNeedingConnection() :
    #print("searchForRoomNeedingConnection()")
    
    tries = 0
    while tries < 50:
        room = finalRooms[random.randrange(len(finalRooms))]
        if len(room.connections) < room.doors :
            addExistingConnections(room)
            return room
        else : tries += 1

    for r in finalRooms :
        if len(r.connections) < r.doors :
            addExistingConnections(r)
            return r

    tries = 0
    while tries < 50:
        room = finalRooms[random.randrange(len(finalRooms))]
        if room.doors == 1 :
            addExistingConnections(room)
            return room
        else : tries += 1

    for r in finalRooms :
        if r.doors == 1 :
            addExistingConnections(r)
            return r

    #all rooms create a loop without a deadend
    return -1

def removeRoom(room) :
    #print("removeRoom()")
    
    finalRooms.remove(room)
    for key, value in room.connections.items() :
        if key == N : key = S
        elif key == E : key = W
        elif key == S : key = N
        elif key == W : key = E
        
        del value.connections[key]
        value.doors = abs(value.doors)

def generateRooms() :
    global finalRooms
    
    fourSidedRooms = [Room.Room(4) for a in range(_4SR)]
    threeSidedRooms = [Room.Room(3) for a in range(_3SR)]
    twoSidedRooms = [Room.Room(2) for a in range(_2SR)]

    availableRooms = fourSidedRooms + threeSidedRooms + twoSidedRooms

    finalRooms = []
    
    #spawn first room
    spawnRoom(0,0, availableRooms)

    while availableRooms :
        #search through rooms to find a room that needs connections
        room = searchForRoomNeedingConnection()
        if room == -1 : return
        elif room.doors == 1 :
            removeRoom(room)
            spawnRoom(room.x, room.y, availableRooms)
            
        #create new rooms by random directions
        createRoomRandomDirection(room, availableRooms)

def findLongestRoute(room, longestList = []) :
    rooms = room.connections

    routeN = longestList + [room]
    routeE = longestList + [room]
    routeS = longestList + [room]
    routeW = longestList + [room]

    if N in rooms :
        if rooms[N] not in longestList :
            routeN = findLongestRoute(rooms[N], routeN)

    if E in rooms :
        if rooms[E] not in longestList :
            routeE = findLongestRoute(rooms[E], routeE)

    if S in rooms :
        if rooms[S] not in longestList :
            routeS = findLongestRoute(rooms[S], routeS)

    if W in rooms :
        if rooms[W] not in longestList :
            routeW = findLongestRoute(rooms[W], routeW)

    return max(routeN, routeE, routeS, routeW, key=len)

def populateRooms() :
    #find longest route
    longRoute = findLongestRoute(getRoom(0,0))
    lenLongRoute = len(longRoute)
    
    #put boss at end
    bossRoom = longRoute[lenLongRoute-1]
    bossRoom.monster = Character.createBossMonster()
    
    #put necessary items in between
    keyRoom = longRoute[random.randrange(int(lenLongRoute/4), int(lenLongRoute * 3 / 4))]
    keyRoom.items = [Items.bossKey]

def createSingleRoom(room) :
    roomMap = """╔═╗
║ ║
╚═╝"""

    north = "╔╩╗"
    south = "╚╦╝"
    east = "╠"
    west = "╣"

    
    try :
        if room.connections["north"] :
            #5 is the length of north
            roomMap = north + roomMap[3:]
    except KeyError :
        pass

    try :
        if room.connections["south"] :
            #5 is the length of south
            roomMap = roomMap[:-3] + south
    except KeyError :
        pass

    try :
        if room.connections["east"] :
            #10 is everything except for the east wall, 11 is after the east wall.
            roomMap = roomMap[:6] + east + roomMap[7:]
    except KeyError :
        pass

    try :
        if room.connections["west"] :
            #6 is everything except for the west wall, 7 is after the west wall.
            roomMap = roomMap[:4] + west + roomMap[5:]
    except KeyError :
        pass

    return roomMap

def displayMap() :
    minX, maxX = finalRooms[0].x, finalRooms[-1].x
    minY = maxY = 0
    display = ""
    
    for room in finalRooms :
        minY, maxY = min(minY, room.y), max(maxY, room.y)
    
    xWidth = maxX - minX + 1
    yWidth = maxY - minY + 1

    emptyCell = """   
   
   """

    displayMap = [[emptyCell for x in range(xWidth)] for y in range(yWidth)]

    for room in finalRooms :
        if room.explored :
            displayMap[room.y - minY][room.x - minX] = createSingleRoom(room)

    for x in displayMap :
        for a in range(3) :
            for y in x :
                for ch in y[4 * a :4 * (a + 1)] :
                    if ch != "\n" : display += ch
            display += "\n"

    return display
