#Robert Higgins
#Project 5
#Version 1.0.0

import Items
import Map
import Room
import Character
import Combat
import Action
import random

previousRoom = None
activeRoom = None
player = Character.Player()

def createWorld() :
    global activeRoom
    print("Loading...")
    Map.generateRooms()
    Map.populateRooms()
    print("Loaded.\n")
    
    activeRoom = Map.getRoom(0,0)
    activeRoom.explored = True

def move(direction) :
    global activeRoom, previousRoom
    
    room = activeRoom.moveFrom(direction)
    if room is None :
        print("There is no door in that direction.")
        return
    elif Items.bossKey not in player.inventory and room.monster and room.monster.level == 4 :
        print("You cannot enter the boss room until you have found the key.")
        return
    elif Items.bossKey in player.inventory and room.monster and room.monster.level == 4 and player.level < 4:
        enter = input("You are a low level entering the boss room. Are you sure you want to enter?").lower()
        if enter == "yes" : pass
        else : return
    elif Items.bossKey in player.inventory and room.monster and room.monster.level == 4 and player.level == 4:
        enter = input("You are about to enter the boss room. Are you sure you want to enter?").lower()
        if enter == "yes" : pass
        else : return

    previousRoom, activeRoom = activeRoom, room
    if room.explored == False :
        room.explored = True
        if room.monster : pass
        elif room.items : room.generateMonster(player)
        else : room.generateContents(player)
    if not room.monster :
        if random.randrange(10) > 6 : room.generateMonster(player)
        
def gameLoop() :
    global activeRoom
    
    while True :
        if activeRoom.monster :
            result = Combat.startCombat(player, activeRoom.monster)
            if result == Combat.SUCCESS :
                print("You have defeated the {}.\n".format(activeRoom.monster.name))
                player.experience += activeRoom.monster.experience
                player.checkExperience()
                if activeRoom.monster.level == 4 :
                    print("And have won the game!")
                    return
                if activeRoom.monster.inventory : Action.transfer(activeRoom.monster, activeRoom, activeRoom.monster.inventory[0])
                activeRoom.monster = None
            elif result == Combat.FAILURE :
                print("You were defeated by the {}.\n".format(activeRoom.monster.name))
                break
            elif result == Combat.RETREAT :
                print("You retreated from the {}.\n".format(activeRoom.monster.name))
                activeRoom = previousRoom
            elif result == Combat.QUIT :
                print("You have quit the game.")
                return

        print(activeRoom.description + "\n")

        for a in range(len(activeRoom.items)-1, -1, -1) :
            item = activeRoom.items[a]
            pickup = input("Do you want to pick up the {}? ".format(item.name)).lower()
            if pickup == "yes" : Action.transfer(activeRoom, player, item)
            elif pickup == "q" :
                print("You have quit the game.")
                return

        m = input("What direction do you want to go? ").lower()
        if m == "q" :
            print("You have quit the game.")
            return
        elif m == "map" :
            print(Map.displayMap())
        else : move(m)

createWorld()

print("""Welcome to this dungeon. At any time you may enter Q to exit the game.
Fully type out the word yes or any directions during movement.
Type MAP for a map.
""")

gameLoop()
        
input()
