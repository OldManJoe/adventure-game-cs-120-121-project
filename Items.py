#Robert Higgins
#Project 5
#Version 1.0.0

from enum import IntEnum
import random

class Item :
    def __init__(self, n, d, l) :
        self.name = n
        self.description = d
        self.level = l

    def use(self, player) :
        pass

class Key(Item) :
    def __init__(self) :
        super().__init__("Key to the Boss Room", "This key opens the door so you can fight the boss.", 0)

class HealingPotion(Item) :
    def __init__(self, hp) :
        super().__init__("Healing Potion", "This potion heals you.", 0)
        self.healingPower = hp

    def use(self, player) :
        player.health += self.healingPower

class Weapon(Item) :
    def __init__(self, name, description, level, ap) :
        super().__init__(name, description, level)
        self.attackPower = ap
        self.equipSlot = EquipSlot.WEAPON

    def use(self, player) :
        replace = player.equipment[3]
        if not replace : player.inventory.append(replace)
        player.equipment[3] = self

class Armor(Item) :
    def __init__(self, name, description, level, dp, es) :
        super().__init__(name, description, level)
        self.defensePower = dp
        self.equipSlot = es

    def use(self, player) :
        replace = player.equipment[equipSlot]
        if not replace : player.inventory.append(replace)
        player.equipment[equipSlot] = self

class EquipSlot(IntEnum) :
    HELMET = 0
    SHIELD = 1
    ARMOR = 2
    WEAPON = 3

bossKey = Key()

basicSword = Weapon("Sword", "The sword you start with, though it's sharpness reminds you more of a club...", 0, 1)
basicShield = Armor("Shield", "The shield you start with, though it's not going to take many hits...", 0, 1, EquipSlot.SHIELD)
basicArmor = Armor("Armor", "The armor you start with, though it isn't as protective as you'd like...", 0, 1, EquipSlot.ARMOR)

heavySword = Weapon("Heavy Sword", "This sword's weight is quite impressive.", 1, 4)
lightHelmet = Armor("Light Helmet", "This is basically a metal hat...", 1, 3, EquipSlot.HELMET)
lightShield = Armor("Light Shield", "This shield will at least take more hits than your last one...", 1, 3, EquipSlot.SHIELD)
lightArmor = Armor("Light Armor", "This armor is both lighter and better able to protect you.", 1, 5, EquipSlot.ARMOR)
level1Items = [heavySword, lightHelmet, lightShield, lightArmor]

def getRandomLevel1Item() :
    item = random.randrange(len(level1Items)) + 2
    if item >= len(level1Items) : item = HealingPotion(random.randrange(5, 11))
    else : item = level1Items[item]
    
    return item

sharpSword = Weapon("Sharp Sword", "This sword's sharpness is quite impressive.", 2, 10)
mediumHelmet = Armor("Medium Helmet", "This helmet is actually protective.", 2, 7, EquipSlot.HELMET)
mediumShield = Armor("Medium Shield", "This small shield will actually stop an attack.", 2, 8, EquipSlot.SHIELD)
mediumArmor = Armor("Medium Armor", "This armor both feels good to wear and safe to wear.", 2, 15, EquipSlot.ARMOR)
level2Items = [sharpSword, mediumHelmet, mediumShield, mediumArmor]

def getRandomLevel2Item() :
    item = random.randrange(len(level2Items)) + 3
    if item >= len(level2Items) : item = HealingPotion(random.randrange(10, 16))
    else : item = level2Items[item]
    
    return item

magicSword = Weapon("Magic Sword", "This sword's magic makes it glow.", 3, 20)
magicHelmet = Armor("Magic Helmet", "This helmet glows with protective power.", 3, 10, EquipSlot.HELMET)
magicShield = Armor("Magic Shield", "Your shield glows with magical energy.", 3, 12, EquipSlot.SHIELD)
magicArmor = Armor("Magic Armor", "Surprisingly this doesn't glow, but you can tell it is magical.", 3, 25, EquipSlot.ARMOR)
level3Items = [magicSword, magicHelmet, magicShield, magicArmor]

def getRandomLevel3Item() :
    item = random.randrange(len(level3Items)) + 4
    if item >= len(level3Items) : item = HealingPotion(random.randrange(15, 21))
    else : item = level3Items[item]
    
    return item
