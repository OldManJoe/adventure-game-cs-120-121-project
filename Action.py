#Robert Higgins
#Project 5
#Version 1.0.0

def transfer(giver, receiver, item) :
    giver.takeItem(item)
    receiver.giveItem(item)
