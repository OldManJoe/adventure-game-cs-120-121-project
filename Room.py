#Robert Higgins
#Project 5
#Version 1.0.0

import random
import Items
import Character

class Room :
    def __init__(self, d) :
        self.doors = d
        self.items = []
        self.monster = None
        self.connections = {}
        self.x = 0
        self.y = 0
        self.explored = False

    def giveItem(self, item) :
        self.items.append(item)

    def takeItem(self, item) :
        self.items.remove(item)

    @property
    def description(self) :
        desc = ""
        if len(self.connections) == 1 :
            desc += "You see a dead end, the only option is to go back through to door behind you, to the "
            for connection in self.connections :
                desc += connection.upper()
        else :
            desc += "You enter the room and see doors to your "
            for connection in self.connections :
                desc += connection.upper() + ", "
            desc = desc[:-2]
        desc += "."
        
        if self.items :
            desc += "\nOn the floor you see "
            for item in self.items :
                desc += item.name + ", "
            desc = desc[:-2] + "."
            
        return desc
            
    def moveFrom(self, d) :
        try :
            return self.connections[d.lower()]
        except KeyError :
            return None

    def generateContents(self, player) :
        self.generateItem(player)
        self.generateMonster(player)
        
    def generateItem(self, player) :
        if random.randrange(10) > 6 :
            if player.level == 1 :
                self.items = [Items.getRandomLevel1Item()]
            elif player.level == 2 :
                self.items = [Items.getRandomLevel2Item()]
            elif player.level == 3 :
                self.items = [Items.getRandomLevel3Item()]

    def generateMonster(self, player) :
        if random.randrange(10) > 4 :
            if player.level == 1 :
                self.monster = Character.Character.createLevel1Monster(player)
            elif player.level == 2 :
                self.monster = Character.Character.createLevel2Monster(player)
            elif player.level >= 3 :
                self.monster = Character.Character.createLevel3Monster(player)
