#Robert Higgins
#Project 5
#Version 1.0.0

from random import randrange
import Items

class Character :
    def __init__(self, n, h, a, d, g, e, l) :
        self.name = n
        self.health = h
        self.attack = a
        self.defense = d
        self.gold = g
        self.experience = e
        self.level = l
        self.inventory = []

    def giveItem(self, item) :
        self.inventory.append(item)

    def takeItem(self, item) :
        self.inventory.remove(item)
    
    @classmethod
    def createLevel1Monster(cls, player) :
        #playerMaxStats hp=10 a=1+1 (2) d=0+0+1+1 (2)
        listOfNames = ["Giant Rat", "Slime", "Giant Spider", "Goblin"]
        return cls(listOfNames[randrange(len(listOfNames))], randrange(1, 6), randrange(player.totalDefense - 1, min(player.totalDefense + 3, 9)), 0, randrange(5), randrange(7, 22, 7), 1)

    @classmethod
    def createLevel2Monster(cls, player) :
        #playerMaxStats hp=20 a=3+4 (7) d=2+3+3+5 (13)
        listOfNames = ["Skeleton", "Giant Slime", "Orc"]
        level2 = cls(listOfNames[randrange(len(listOfNames))], randrange(5, 10), randrange(max(player.totalDefense - 2, 7), player.totalDefense + 4), randrange(2, 5), randrange(10, 36, 5), randrange(9, 28, 9), 2)
        level2.inventory = [Items.getRandomLevel2Item()]
        return level2

    @classmethod
    def createLevel3Monster(cls, player) :
        #playerMaxStats hp=30 a=5+10 (15) d=4+7+8+15 (34)
        listOfNames = ["Giant", "Vampire", "Rock Golem"]
        level3 = cls(listOfNames[randrange(len(listOfNames))], randrange(10, 21), randrange(player.totalDefense - 3, player.totalDefense + 5), randrange(10, 13), randrange(20, 51, 10), randrange(11, 34, 11), 3)
        level3.inventory = [Items.getRandomLevel3Item()]
        return level3
    
    @classmethod
    def createBossMonster(cls) :
        #playerMaxStats hp=40 a=7+20 (27) d=6+10+12+25 (53)
        listOfNames = ["Dragon", "Giant King"]
        return cls(listOfNames[randrange(len(listOfNames))], randrange(30, 41), randrange(55, 61), randrange(22, 25), 0, 0, 4)        
        
class Player(Character) :
    def __init__(self) :
        super().__init__("", 10, 1, 0, 0, 0, 1)
        self.equipment = [None, Items.basicShield, Items.basicArmor, Items.basicSword]

    def giveItem(self, item) :
        print(item.description + "\n")

        if type(item) is Items.HealingPotion :
            self.health += item.healingPower
            print("You were healed by {} points.".format(item.healingPower))
            return

        try :
            currentEquipment = self.equipment[item.equipSlot]
        
            if currentEquipment.level < item.level :
                self.inventory.append(currentEquipment)
                self.equipment[item.equipSlot] = item
            else :
                self.inventory.append(item)
        except AttributeError :
            self.inventory.append(item)

    @property
    def totalDefense(self) :
        defense = self.defense
        
        for a in range(3) :
            if self.equipment[a] : defense += self.equipment[a].defensePower

        return defense

    @property
    def totalAttack(self) :
        attack = self.attack
        
        if self.equipment[Items.EquipSlot.WEAPON] : attack += self.equipment[Items.EquipSlot.WEAPON].attackPower

        return attack

    def checkExperience(self) :
        if self.level == 4 : return
        elif self.level == 1 and self.experience > 100 :
            self.levelUp()
        elif self.level == 2 and self.experience > 200 :
            self.levelUp()
        elif self.level == 3 and self.experience > 300 :
            self.levelUp()

    def levelUp(self) :
        self.level += 1
        self.health += 10
        self.attack += 2
        self.defense += 2

