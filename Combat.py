#Robert Higgins
#Project 5
#Version 1.0.0

import random

SUCCESS = 1
RETREAT = 0
FAILURE = -1
QUIT = -2

def startCombat(player, monster) :
    print("\nYou have encountered a {}\n".format(monster.name))
    
    if random.randrange(10) > 5 : isPlayerTurn = True
    else : isPlayerTurn = False

    while True :
        if isPlayerTurn :
            attack = input("Do you wish to attack? ").lower()
            if attack == "yes" :
                damage = player.totalAttack - monster.defense
                damage = damage if damage > 0 else 1
                monster.health -= damage
                print("You dealt {} damage to the {}.\n".format(damage, monster.name))
            elif attack == "q" : return QUIT
            else : return RETREAT
        else :
            damage = monster.attack - player.totalDefense
            damage = damage if damage > 0 else 0
            player.health -= damage
            print("You were hurt for {} damage by the {}.\n".format(damage, monster.name))

        isPlayerTurn = not isPlayerTurn
        
        if player.health <= 0 : return FAILURE
        elif monster.health <= 0 : return SUCCESS
